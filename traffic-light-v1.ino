int RED = 11;
int YLW = 12;
int GRN = 13;

bool ON = false;
bool OFF = true;

void red(bool onOrOff) {
  digitalWrite(RED, onOrOff);
}

void amber(bool onOrOff) {
  digitalWrite(YLW, onOrOff);
}

void green(bool onOrOff) {
  digitalWrite(GRN, onOrOff);
}

void R() {
  red(ON);
  amber(OFF);
  green(OFF);
}

void G() {
  red(OFF);
  amber(OFF);
  green(ON);
}

void A() {
  red(OFF);
  amber(ON);
  green(OFF);
}

void (*RGA[])() = { R, G, A};

void setup() {
  // put your setup code here, to run once:
  pinMode(RED, OUTPUT);
  pinMode(YLW, OUTPUT);
  pinMode(GRN, OUTPUT);

  randomSeed(analogRead(0));
}

void germanPattern() {
    // German light pattern
    R();

    delay(3500);

    red(ON);
    amber(ON);
    green(OFF);

    delay(1000);

    G();

    delay(5000);

    A();

    delay(1500);

    R();

    delay(2000);
}

void dutchPattern() {
    // Dutch light pattern
    R();

    delay(5000);

    G();

    delay(5000);

    A();

    delay(1500);

    R();

    delay(2000);
}

void racetrackPattern() {
 // Racetrack pattern
    R();

    delay(1000);

    A();

    delay(1000);

    G();

    delay(2000);
}

void discoAllLights() {
  int i = 0;

  while (i <= 3) {

    red(ON);
    amber(ON);
    green(ON);

    delay(700);

    red(OFF);
    amber(OFF);
    green(OFF);

    delay(700);

    i++;
  }
}

void leftToRight() {
  R();

  delay(500);

  A();

  delay(500);

  G();

  delay(500);
}

void rightToLeft() {
  G();

  delay(500);

  A();

  delay(500);

  R();

  delay(500);
}

void middleToSide() {
  int i = 0;

  while (i <= 3) {
    A();

    delay(500);

    red(ON);
    amber(OFF);
    green(ON);

    delay(500);
    i++;
  }
}

void pingPong() {
  int i = 0;

  while (i <= 5) {
    G();

    delay(500);

    A();

    delay(500);

    R();

    delay(1000);

    A();

    delay(500);

    G();

    delay(500);

    i++;
  }
}

void wild() {
  int i = 0;

  while (i <= 10) {
    int generatedNum = random(0, sizeof(RGA)/sizeof(RGA[0]));

    RGA[generatedNum]();

    delay(600);
    i++;
  }
}

void (*functions[])() = { dutchPattern, germanPattern, racetrackPattern, discoAllLights, leftToRight, rightToLeft, middleToSide, pingPong, wild };

void loop() {
  int index = random(0, sizeof(functions)/sizeof(functions[0]));
  
  // Call the function at the random index
  functions[index]();
}