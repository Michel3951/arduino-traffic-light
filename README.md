# Arduino Traffic Light
Controlling a real traffic light using an arduino uno R3 and a 5v 4 channel relay.


For now I will just put the C++ code here. I will probably add a wiring diagram later (forgive me lord if I don't). The traffic light runs on 240 volts using three 4 watt LED light bulbs.

## Requirements
- Arduino UNO R3 board
- 5V 4-Channel Relay interface board (250V AC, 10A)
- A traffic light (yes, really)
- 5 jumper cables (male to female)
- Electrical wiring
- 7-12v DC power supply (2.1mm jack)

Kuddos to [Ibrar Ayyub](https://duino4projects.com/traffic-signal-stop-light-wiring-arduino-controller/) for the original wiring diagram.
![wiring diagram](https://i.imgur.com/F4C4aWr.png)